using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelMenu : MonoBehaviour
{
	private class Item
	{
		public int index;
		public int indexGroup;
		public float angle;
		public readonly float offsetAngle;
		public bool interactable;
	}

	[SerializeField] private float _radiusIcon = 340f;
	[SerializeField] private float _radiusText = 480f;
	[SerializeField] private float _rotationLerpTime = 0.4f;

	[Header("Gizmos")]
	[SerializeField] private bool _showGizmos = true;
	[SerializeField] private float _gizmosSize = 15f;


	//protected override void Setup()
	//{
	//	base.Setup();

	//	for (int i = 0; i < _subItems.Length; i++)
	//	{
	//		_subItems[i] = new List<Item>();
	//	}
	//	_mainModule = MainModule.instance;

	//	_menuRing = XButton.Create(gameObject, "bg", 840, 840, pBackgroundImgPath: "Images/menu-ring");
	//	_menuRing.onGesture += HandleGestureOnRing;
	//	RegisterAlphaChild(_menuRing);

	//	_currentIndex = 0;
	//	CreateMenuItem(0, 2, "Images/icon-design", 0);
	//	CreateMenuItem(1, 0, "Images/icon-material", 40);
	//	CreateMenuItem(2, 1, "Images/icon-demontage", 80);

	//	CreateMenuItem(3, 2, "Images/icon-design", 120);
	//	CreateMenuItem(4, 0, "Images/icon-material", 160);
	//	CreateMenuItem(5, 1, "Images/icon-demontage", -160);

	//	CreateMenuItem(6, 2, "Images/icon-design", -120);
	//	CreateMenuItem(7, 0, "Images/icon-material", -80);
	//	CreateMenuItem(8, 1, "Images/icon-demontage", -40);


	//	_mainModule.onCurrentMeshControllerChanged += HandleMeshControllerChanged;
	//	_mainModule.onLanguageChanged += HandleLanguageChanged;
	//}

	//private void HandleLanguageChanged(MainModule.Language language)
	//{
	//	if (MainModule.instance.currentMeshController != null)
	//	{
	//		HandleMeshControllerChanged(MainModule.instance.currentMeshController);
	//	}
	//}


	//private void HandleMeshControllerChanged(MeshController meshcontroller)
	//{
	//	var flapCards = meshcontroller.flapCards;
	//	var contentItems = meshcontroller.contentItems;

	//	_twoMode = (contentItems.Length == 2);

	//	foreach (var item in items)
	//	{
	//		item.label.textLabel.text = "";
	//		item.button.background.alpha = 0;
	//	}

	//	for (int i = 0; i < flapCards.Count; i++)
	//	{
	//		foreach (var item in _subItems[i])
	//		{
	//			item.label.textLabel.text = flapCards[i].menuWheelTitle;
	//			item.button.background.alpha = 1;
	//			item.button.SetBackground(contentItems[i].icon);
	//		}
	//	}

	//	//WORKAROUND: in twoMode the intial rotation for index 0 did not work
	//	//this forces the wheel into a valid state
	//	if (_twoMode)
	//	{
	//		SetIndexGroup(1, false);
	//	}
	//}



	//float _deltaAngle;
	//private float _deltaValue = 0;

	//private FloatRange _valueRange = new FloatRange(0, 1);
	//private FloatRange _angleRange = new FloatRange(0, 360);

	//float startAngle;

	//private void HandleGestureOnRing(object obj, GestureEventArgs e)
	//{
	//	if (PartView.SubState != PartView.SubViewState.None)
	//	{
	//		return;
	//	}

	//	if (_mainModule.currentMeshController.contentItems.Length == 1)
	//	{
	//		return;
	//	}

	//	switch (e.eventID)
	//	{
	//		case GestureEvent.DragStart:
	//			StopCoroutine(_coroutineShowButtonNames);
	//			ShowButtonNames();
	//			break;
	//		case GestureEvent.Drag:
	//			_deltaAngle = AngleFromPosition(e.globalPos) - AngleFromPosition(e.globalPosPrev);
	//			RotateRing(_deltaAngle);
	//			break;
	//		case GestureEvent.DragEnd:
	//			StartCoroutine(IEStopRotation());
	//			//HideButtonNames(3);
	//			break;
	//	}
	//}

	//private IEnumerator IEStopRotation()
	//{
	//	// Rotate longer when delta angle is bigger (drag was faster)
	//	float lerpTime = Mathf.Clamp(Mathf.Abs(0.2f * _deltaAngle), 0.2f, 0.5f);
	//	float snapLerpTime = 0.1f;
	//	float currentLerpTime = 0f;
	//	float start = _deltaAngle;
	//	float end = 0;
	//	bool finalize = false;

	//	while (currentLerpTime < lerpTime)
	//	{
	//		currentLerpTime += Time.deltaTime;
	//		if (currentLerpTime > lerpTime) currentLerpTime = lerpTime;

	//		float t = currentLerpTime / lerpTime;
	//		t = Easings.instance.Interpolate(t, Easings.Function.SineInOut);

	//		if (!finalize)
	//		{
	//			float deltAngle = Mathf.Lerp(start, end, t); // Value represents the speed of the rotation of the wheel
	//			RotateRing(deltAngle);

	//			// Start finalizing (snap to position)
	//			if (deltAngle < 2 || currentLerpTime <= snapLerpTime)
	//			{
	//				start = _menuRing.rotZ;
	//				int rounded = Mathf.RoundToInt(_menuRing.rotZ / 40);
	//				end = rounded * 40;
	//				lerpTime = snapLerpTime;
	//				finalize = true;
	//			}
	//		}
	//		else
	//		{
	//			float angle = Mathf.Lerp(start, end, t);
	//			RotateRing(angle - _menuRing.rotZ);
	//			//Debug.Log("<color=lime>"+ _menuRing.rotZ + "</color>");
	//		}
	//		yield return null;
	//	}

	//	UpdateSelectedIndexGroupAccordingToWheelRot();
	//	yield return null;
	//}

	private float AngleFromPosition(Vector3 pos)
	{
		Vector3 dir = (pos - transform.position).normalized;
		return Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
	}

	//private bool AngleApproximatesPosition()
	//{
	//	int rounded = Mathf.RoundToInt(_menuRing.rotZ / 40);
	//	int nextSnapPosition = rounded * 40;
	//	float current = _menuRing.rotZ;
	//	return current.Approximates(nextSnapPosition, 0.1f);
	//}

	//private void Update()
	//{
	//	//TEST
	//	if (Input.GetKeyDown(KeyCode.A))
	//	{
	//		Open = !open;
	//	}

	//	UpdateTextAlpha();
	//}

	//private void UpdateTextAlpha()
	//{
	//	foreach (var item in _items)
	//	{
	//		item.label.alphaMax = Utilities.Float.ValueMapper(item.label.x, 150, 350, 0, 1);
	//	}
	//}



	//private Item CreateMenuItem(int index, int indexGroup, string iconPath, float angle)
	//{
	//	XButton btn = XButton.Create(gameObject, "btn_" + index, _widthSmall, _widthSmall, "", pBackgroundImgPath: iconPath);
	//	btn.localPosition = GetPositionByAngle(angle, _radiusIcon);
	//	btn.onGesture += HandleGestureOnRing;

	//	btn.onTap += (o) =>
	//	{
	//		if (PartView.SubState != PartView.SubViewState.None)
	//			return;
	//		SetIndexGroup(indexGroup, true);
	//	};

	//	XButton txt = XButton.Create(gameObject, "txt_" + index, Mathf.RoundToInt(2.5f * _widthSmall), Mathf.RoundToInt(2f * _widthSmall), name.ToString(), FontModule.defaultFont, 40);
	//	txt.textColorActive = txt.textColorPressed = Color.white;
	//	txt.localPosition = GetPositionByAngle(angle, _radiusText);
	//	txt.textAlignment = TMPro.TextAlignmentOptions.MidlineLeft;
	//	txt.textLabel.x = 0;
	//	txt.onTap += (o) => btn.Tap();
	//	txt.onGesture += HandleGestureOnRing;

	//	Item item = new Item(index, btn, txt, indexGroup, angle);
	//	RegisterAlphaChild(btn, txt);
	//	_items.Add(item);

	//	_subItems[indexGroup].Add(item);

	//	return item;
	//}


	//private void RotateRing(float angle)
	//{

	//	float menuRingRot = 0;
	//	if (_twoMode) menuRingRot = (_menuRing.rotZ + angle).Between(40, 80);
	//	else
	//		menuRingRot = _menuRing.rotZ + angle;

	//	_menuRing.rotZ = menuRingRot;

	//	for (int i = 0; i < _items.Count; i++)
	//	{
	//		Item item = _items[i];
	//		item.angle = _menuRing.rotZ;
	//		item.button.localPosition = GetPositionByAngle(item.angle + item.offsetAngle, _radiusIcon);
	//		item.label.localPosition = GetPositionByAngle(item.angle + item.offsetAngle, _radiusText);
	//	}
	//}

	//public void SetIndexGroup(int indexGroup, bool dispatch)
	//{
	//	if (_selectedIndexGroup == indexGroup)
	//	{
	//		return;
	//	}
	//	_selectedIndexGroup = indexGroup;
	//	RadialMoveTo(indexGroup);
	//	if (dispatch) onSelectedIndexGroupChanged?.Invoke(_selectedIndexGroup);
	//	ShowButtonNamesForSeconds();
	//}

	//private void RadialMoveTo(int indexGroup)
	//{
	//	if (_mainModule.currentMeshController.contentItems.Length <= indexGroup) return;
	//	StartCoroutine(IERadialMoveTo(indexGroup));
	//}

	//private IEnumerator IERadialMoveTo(int indexGroup)
	//{
	//	float lerpTime = _rotationLerpTime;
	//	float currentLerpTime = 0f;

	//	float start = _menuRing.rotZ;


	//	//figure out how we have to rotate
	//	//switch (previousGroup)
	//	//{
	//	//	case 2:
	//	//		if (indexGroup == 0) end = start - 40;
	//	//		else if (indexGroup == 1) end = start + 40;
	//	//		break;
	//	//	case 0:
	//	//		if (indexGroup == 2) end = start + 40;
	//	//		else if (indexGroup == 1) end = start - 40;
	//	//		break;
	//	//	case 1:
	//	//		if (indexGroup == 0) end = start + 40;
	//	//		else if (indexGroup == 2) end = start - 40;
	//	//		break;
	//	//}

	//	float end = -40f * (indexGroup + 1);

	//	float dif = end- start;
	//	while (Mathf.Abs(dif) > 60f)
	//	{
	//		dif -= Mathf.Sign(dif) * 120f;
	//	}
	//	end =  start + dif;

	//	//lerp there
	//	while (currentLerpTime < lerpTime)
	//	{
	//		currentLerpTime += Time.deltaTime;
	//		if (currentLerpTime > lerpTime) currentLerpTime = lerpTime;

	//		// percentage
	//		float t = currentLerpTime / lerpTime;
	//		t = Easings.instance.Interpolate(t, Easings.Function.SineInOut);

	//		// lerp ring
	//		float angle = Mathf.Lerp(start, end, t);
	//		RotateRing(angle - _menuRing.rotZ);

	//		yield return null;
	//	}

	//	yield return null;
	//}

	private float AngleDifference(float angle1, float angle2)
	{
		float diff = (angle2 - angle1 + 180) % 360 - 180;
		return diff < -180 ? diff + 360 : diff;
	}

	//private void UpdateSelectedIndexGroupAccordingToWheelRot()
	//{
	//	if (_menuRing.rotZ.Approximates(0, 10) || _menuRing.rotZ.Approximates(120, 10) || _menuRing.rotZ.Approximates(240, 10))
	//	{
	//		SetIndexGroup(2, true);
	//	}

	//	if (_menuRing.rotZ.Approximates(80, 10) || _menuRing.rotZ.Approximates(200, 10) || _menuRing.rotZ.Approximates(320, 10))
	//	{
	//		SetIndexGroup(0, true);
	//	}

	//	if (_menuRing.rotZ.Approximates(40, 10) || _menuRing.rotZ.Approximates(160, 10) || _menuRing.rotZ.Approximates(280, 10))
	//	{
	//		SetIndexGroup(1, true);
	//	}
	//}

	//public void ShowButtonNamesForSeconds(float duration = 2f)
	//{
	//	if (_coroutineShowButtonNames != null) StopCoroutine(_coroutineShowButtonNames);
	//	_coroutineShowButtonNames = StartCoroutine(IEShowButtonNamesForSeconds(duration));
	//}

	//private void ShowButtonNames()
	//{
	//	Tukan.EaseType easeIn = Tukan.EaseType.CubicOut;
	//	Tukan.EaseType easeOut = Tukan.EaseType.CubicIn;
	//	float fadeInTime = 0.15f;
	//	float fadeOutTime = 1f;

	//	foreach (var i in _items)
	//	{
	//		i.label.AlphaTo(_selectedIndexGroup == i.indexGroup ? 1 : 0.35f, fadeInTime, 0, easeIn);
	//	}
	//}

	//private void HideButtonNames(float delay = 0)
	//{
	//	Tukan.EaseType easeIn = Tukan.EaseType.CubicOut;
	//	Tukan.EaseType easeOut = Tukan.EaseType.CubicIn;
	//	float fadeInTime = 0.15f;
	//	float fadeOutTime = 1f;

	//	// All labels fade out
	//	foreach (var i in _items)
	//	{
	//		i.label.AlphaTo(0, fadeOutTime, delay, easeOut);
	//	}
	//}

	//private IEnumerator IEShowButtonNamesForSeconds(float duration)
	//{
	//	Tukan.EaseType easeIn = Tukan.EaseType.CubicOut;
	//	Tukan.EaseType easeOut = Tukan.EaseType.CubicIn;
	//	float fadeInTime = 0.15f;
	//	float fadeOutTime = 1f;

	//	// Icons / buttons
	//	//_btnDesign.AlphaTo(1, fadeInTime);
	//	//_btnDemontage.AlphaTo(1, fadeInTime);
	//	//_btnMaterial.AlphaTo(1, fadeInTime);
	//	foreach (var i in _items)
	//	{
	//		i.button.AlphaTo(1, fadeInTime);
	//	}

	//	ShowButtonNames();

	//	yield return new WaitForSeconds(duration);

	//	HideButtonNames();

	//}

	//private void Show()
	//{
	//	MoveToX(-1520, 0.25f);
	//	ShowButtonNamesForSeconds();
	//}

	//private void Hide(bool full = false)
	//{
	//	if (!full)
	//	{
	//		MoveToX(-1631, 0.25f);

	//		foreach (var i in _items)
	//		{
	//			i.button.AlphaTo(_selectedIndexGroup == i.indexGroup ? 1 : 0, 0.25f);
	//			i.button.AlphaTo(_selectedIndexGroup == i.indexGroup ? 1 : 0, 0.25f);
	//		}

	//	}
	//	else
	//	{
	//		MoveToX(-1900f, 0.25f);
	//	}
	//}

	//private void HideAll()
	//{
	//	Hide(true);
	//}



	//private Vector3 GetPositionByAngle(float angle, float radius)
	//{
	//	float x = 0 + radius * Mathf.Cos(angle * Mathf.PI / 180f);
	//	float y = 0 + radius * Mathf.Sin(angle * Mathf.PI / 180f);
	//	float z = 0;

	//	return new Vector3(x, y, z);
	//}

	//private IEnumerator IELabelMoveOutAnimation(XButton label)
	//{
	//	label.AlphaTo(0, _rotationLerpTime * 0.15f);
	//	yield return new WaitForSeconds(0.8f * _rotationLerpTime);
	//	label.AlphaTo(0.25f, _rotationLerpTime * 0.2f);
	//}

	private void OnDrawGizmosSelected()
	{
		
	}
}
