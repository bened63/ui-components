using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Playables;

public class DragArea : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
	[SerializeField] private bool showDebugComments = false;

	public Action onDragBeginEvent;
	public Action<PointerEventData> onDragEvent;
	public Action onDragEndEvent;

	public void OnBeginDrag(PointerEventData eventData)
	{
		onDragBeginEvent?.Invoke();
		if (showDebugComments) Debug.Log("<color=lime>OnBeginDrag</color>");
	}

	public void OnDrag(PointerEventData eventData)
	{
		onDragEvent?.Invoke(eventData);
		if (showDebugComments) Debug.Log("<color=lime>OnDrag " + eventData.delta + "</color>");
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		onDragEndEvent?.Invoke();
		if (showDebugComments) Debug.Log("<color=lime>OnEndDrag</color>");
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		if (showDebugComments) Debug.Log("<color=lime>OnPointerDown</color>");
	}
}
