using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageSlideshow : MonoBehaviour
{
	[SerializeField] private int _currentIndex = 0;
	private float _lerpTime = 1;

	[Space(10)]
	[SerializeField] private GameObject _navigationPoint;

	[SerializeField] private Transform _navigationPointParent;
	[SerializeField] private Sprite[] _sprites;

	[Space(10)]
	[SerializeField] private RectTransform[] _imageContainer;

	[Space(10)]
	[Header("Debugging Options")]
	[SerializeField] private bool _previous = false;

	[SerializeField] private bool _next = false;
	[SerializeField] private bool _update = false;
	[SerializeField] private float _imageWidth = -1;

	[SerializeField] private float _xLeft = -1;
	[SerializeField] private float _xCenter = -1;
	[SerializeField] private float _xRight = -1;
	[SerializeField] private float _distance = -1;

	[SerializeField] private bool _isLerping = false;

	private GameObject _navPointCursor;
	private List<GameObject> _navPointList;

	public int currentIndex
	{
		get => _currentIndex;
		set
		{
			bool next = value > _currentIndex;

			// Special case if you are at the FIRST app/sprite
			if (_currentIndex == 0 && value == _sprites.Length - 1)
			{
				next = false;
			}

			// Special case if you are at the LAST app/sprite
			if (_currentIndex == _sprites.Length - 1 && value == 0)
			{
				next = true;
			}

			if (!_isLerping && _currentIndex != value)
			{
				UpdateImageSprites();
				if (next) NextIndex();
				else PreviousIndex();

				//_scaledIndex = GetSpriteIndex(value);
				_currentIndex = value;
			}
		}
	}

	public float lerpTime { get => _lerpTime; set => _lerpTime = value; }

	// Start is called before the first frame update
	private void Start()
	{
		//// Set sprites
		//_imageContainer[0].transform.GetComponentInChildren<Image>().sprite = _sprites[GetIndex(_currentIndex - 1)]; // left
		//_imageContainer[1].transform.GetComponentInChildren<Image>().sprite = _sprites[GetIndex(_currentIndex)]; // center
		//_imageContainer[2].transform.GetComponentInChildren<Image>().sprite = _sprites[GetIndex(_currentIndex + 1)]; // right

		// Set container positions
		float containerWidth = _imageContainer[0].rect.size.x;
		_imageContainer[0].localPosition = _imageContainer[1].localPosition - new Vector3(containerWidth, 0, 0);
		_imageContainer[2].localPosition = _imageContainer[1].localPosition + new Vector3(containerWidth, 0, 0);

		// Set navigation points
		InitNavigationsPoints();

		// Set images values: x-positions and distance between images
		InitImageValues();

		UpdateImageSprites();
	}

	private void Update()
	{
		//if (_previous)
		//{
		//	currentIndex = _currentIndex - 1;
		//	_previous = false;
		//}

		//if (_next)
		//{
		//	currentIndex = _currentIndex + 1;
		//	_next = false;
		//}
	}

	private void InitNavigationsPoints()
	{
		// Navigation points
		_navPointList = new List<GameObject>();
		for (int i = 0; i < _sprites.Length; i++)
		{
			GameObject go = Instantiate(_navigationPoint, _navigationPointParent);
			go.GetComponent<Image>().color = new Color32(14, 14, 14, 255);
			_navPointList.Add(go);
		}

		// Cursor
		//_navPointCursor = Instantiate(_navigationPoint, _navigationPointParent);
		//Destroy(_navPointCursor.GetComponent<Flexbox4Unity.FlexItem>());
		//_navPointCursor.GetComponent<RectTransform>().sizeDelta = _navPointList[0].GetComponent<RectTransform>().sizeDelta;

		Invoke("UpdateNavPointCursor", 0.05f);
	}

	private void UpdateNavPointCursor()
	{
		_navPointCursor.transform.position = _navPointList[_currentIndex].transform.position;
	}

	public void PreviousIndex()
	{
		StartCoroutine(IESlide(true));
	}

	public void NextIndex()
	{
		StartCoroutine(IESlide(false));
	}

	private Image GetImageOfChild(RectTransform rect)
	{
		foreach (Image i in rect.transform.GetComponentsInChildren<Image>())
		{
			if (i.transform != rect.transform) return i;
		}
		return null;
	}

	private void InitImageValues()
	{
		if (_imageContainer.Length == 0)
		{
			return;
		}

		_xLeft = _imageContainer[0].position.x;
		_xCenter = _imageContainer[1].position.x;
		_xRight = _imageContainer[2].position.x;
		_distance = Mathf.Abs(_xRight - _xCenter);
	}

	private IEnumerator IESlide(bool goLeft = false)
	{
		_isLerping = true;

		float currentLerpTime = 0f;
		float lerpTime = _lerpTime;
		float offset = -1 * _distance;
		if (goLeft) offset = _distance;

		// Start values
		float startLeft = _imageContainer[0].position.x;
		float startCenter = _imageContainer[1].position.x;
		float startRight = _imageContainer[2].position.x;
		Vector3 startNavCursor = _navPointCursor.transform.position;

		// End values
		float endLeft = startLeft + offset;
		float endCenter = startCenter + offset;
		float endRight = startRight + offset;
		Vector3 endNavCursor = _navPointList[goLeft ? GetSpriteIndex(_currentIndex - 1) : GetSpriteIndex(_currentIndex + 1)].transform.position;

		while (currentLerpTime < lerpTime)
		{
			//increment timer once per frame
			currentLerpTime += Time.deltaTime;
			if (currentLerpTime > lerpTime)
			{
				currentLerpTime = lerpTime;
			}

			//lerp!
			float t = currentLerpTime / lerpTime;
			t = EaseInOutBack(t);

			//images
			_imageContainer[0].position = LerpContainerPosition(_imageContainer[0].position, startLeft, endLeft, t);
			_imageContainer[1].position = LerpContainerPosition(_imageContainer[1].position, startCenter, endCenter, t);
			_imageContainer[2].position = LerpContainerPosition(_imageContainer[2].position, startRight, endRight, t);

			//navigation
			_navPointCursor.transform.position = Vector3.Lerp(startNavCursor, endNavCursor, t);

			yield return null;
		}

		// Update position of image containers
		UpdateImagePositions();
		UpdateImageSprites();

		// lerp finished
		_isLerping = false;
	}

	private int GetSpriteIndex(int i)
	{
		int result = 0;
		if (i < 0)
		{
			int mod = Mathf.Abs(i) % _sprites.Length;
			result = mod == 0 ? 0 : _sprites.Length - mod;
		}
		else
		{
			int mod = Mathf.Abs(i) % _sprites.Length;
			result = mod;
		}
		return Mathf.Clamp(result, 0, _sprites.Length - 1);
	}

	private void UpdateImagePositions()
	{
		// Sprites
		int leftIndex = GetSpriteIndex(_currentIndex - 1);
		int rightIndex = GetSpriteIndex(_currentIndex + 1);

		foreach (RectTransform rect in _imageContainer)
		{
			if (rect.position.x < _xLeft * 1.5f) // check if image must be moved to the right
			{
				// move to the right
				rect.position = new Vector3(_xRight, rect.position.y, rect.position.z);
				GetImageOfChild(rect).sprite = _sprites[rightIndex];
				//rect.transform.GetComponentInChildren<Image>().sprite = _sprites[rightIndex];
			}
			else if (rect.position.x > _xRight * 1.5f) // check if image must be moved to the left
			{
				// move to the left
				rect.position = new Vector3(_xLeft, rect.position.y, rect.position.z);
				//Debug.Log("<color=lime>=================== leftIndex " + leftIndex + "</color>");
				GetImageOfChild(rect).sprite = _sprites[leftIndex];
				//rect.transform.GetComponentInChildren<Image>().sprite = _sprites[leftIndex];
			}
		}
	}

	private void UpdateImageSprites()
	{
		// Sprites
		int leftIndex = GetSpriteIndex(_currentIndex - 1);
		int rightIndex = GetSpriteIndex(_currentIndex + 1);

		foreach (RectTransform rect in _imageContainer)
		{
			if (rect.position.x < _xCenter - 1)
			{
				// move to the right
				GetImageOfChild(rect).sprite = _sprites[leftIndex];
				//rect.transform.GetComponentInChildren<Image>().sprite = _sprites[leftIndex];
			}
			else if (rect.position.x > _xCenter + 1)
			{
				// move to the left
				GetImageOfChild(rect).sprite = _sprites[rightIndex];
				//rect.transform.GetComponentInChildren<Image>().sprite = _sprites[rightIndex];
			}
			else
			{
				GetImageOfChild(rect).sprite = _sprites[_currentIndex];
				Debug.Log("<color=lime>=============> _currentIndex = " + _currentIndex + "</color>");
				//rect.transform.GetComponentInChildren<Image>().sprite = _sprites[_currentIndex];
			}
		}
	}

	private Vector3 LerpContainerPosition(Vector3 position, float start, float end, float t)
	{
		Vector3 pos = position;
		pos.x = Mathf.LerpUnclamped(start, end, t);
		return pos;
	}

	private float EaseInOutBack(float x)
	{
		float c1 = 1.70158f - 0.9f; // back value (default: 1.70158f)
		float c2 = c1 * 1.525f;
		return x < 0.5f ? (Mathf.Pow(2 * x, 2) * ((c2 + 1) * 2 * x - c2)) / 2 : (Mathf.Pow(2 * x - 2, 2) * ((c2 + 1) * (x * 2 - 2) + c2) + 2) / 2;
	}
}