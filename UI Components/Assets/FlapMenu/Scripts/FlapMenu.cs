using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class FlapMenu : MonoBehaviour
{
    public enum PinDirection
	{
        Left, Right, Top, Bottom
	}

    [Header("References")]
    [SerializeField] private RectTransform _rect;

    [Header("Settings")]
    [SerializeField] private bool _openAtStart = true;
    [SerializeField, Min(0f)] private float _lerpTime = 1f;
    [SerializeField, Range(0f, 100f)] private float _widthPercent = 20;
    [SerializeField, Range(0f, 100f)] private float _heightPercent = 90;
    [SerializeField] private PinDirection _pinDirection = PinDirection.Right;

    [Header("Infos")]
    private bool _isOpen = true;

    private Coroutine _openCoroutine;
    private int _screenWidth = 0;
    private int _screenHeight = 0;
    private Dictionary<PinDirection, Vector2> _pinPositionsOpened;
    private Dictionary<PinDirection, Vector2> _pinPositionsClosed;

    public bool isOpen { get => _isOpen; }
	public PinDirection pinDirection { get => _pinDirection; }

	// Start is called before the first frame update
	void Start()
    {
        Init();
    }

    // Update is called once per frame
    void Update()
    {
    }

	private void OnValidate()
	{
        Init();
	}

	public void Init()
    {
        // RectTransform
        _rect = GetComponent<RectTransform>();

        // Size
        if (_rect != null)
        {
#if UNITY_EDITOR
            string[] res = UnityStats.screenRes.Split('x');
            _screenWidth = int.Parse(res[0]);
            _screenHeight = int.Parse(res[1]);
#else
            screenWidth = Screen.width;
            screenHeight = Screen.height;
#endif
            _rect.sizeDelta = new Vector2(_screenWidth * _widthPercent * 0.01f, _screenHeight * _heightPercent * 0.01f);

            // Positions
            _pinPositionsOpened = new Dictionary<PinDirection, Vector2>();
            _pinPositionsOpened.Add(PinDirection.Left, new Vector2(-0.5f * _screenWidth - 0.5f * _rect.sizeDelta.x, 0));
            _pinPositionsOpened.Add(PinDirection.Right, new Vector2(0.5f * _screenWidth + 0.5f * _rect.sizeDelta.x, 0));
            _pinPositionsOpened.Add(PinDirection.Top, new Vector2(0, 0.5f * _screenHeight + 0.5f * _rect.sizeDelta.y));
            _pinPositionsOpened.Add(PinDirection.Bottom, new Vector2(0, -0.5f * _screenHeight - 0.5f * _rect.sizeDelta.y));

            _pinPositionsClosed = new Dictionary<PinDirection, Vector2>();
            _pinPositionsClosed.Add(PinDirection.Left, new Vector2(-0.5f * _screenWidth + 0.5f * _rect.sizeDelta.x, 0));
            _pinPositionsClosed.Add(PinDirection.Right, new Vector2(0.5f * _screenWidth - 0.5f * _rect.sizeDelta.x, 0));
            _pinPositionsClosed.Add(PinDirection.Top, new Vector2(0, 0.5f * _screenHeight - 0.5f * _rect.sizeDelta.y));
            _pinPositionsClosed.Add(PinDirection.Bottom, new Vector2(0, -0.5f * _screenHeight + 0.5f * _rect.sizeDelta.y));

            if (_openAtStart) Open();
            else Close();
        }
    }

    private IEnumerator IEOpen(bool open = true)
	{
        _isOpen = open;
        float lerpTime = _lerpTime;
        float currentLerpTime = 0;
        Vector2 start = _rect.localPosition;
        Vector2 end = start;

        if (!open) end = _pinPositionsOpened[_pinDirection];
        else end = _pinPositionsClosed[_pinDirection];

        while (currentLerpTime < lerpTime)
		{
            // increment timer once per frame
            currentLerpTime += Time.deltaTime;
            if (currentLerpTime > lerpTime)
            {
                currentLerpTime = lerpTime;
            }

            // lerp!
            float t = currentLerpTime / lerpTime;
            t = t * t * t * (t * (6f * t - 15f) + 10f);
			_rect.localPosition = Vector3.LerpUnclamped(start, end, t);

            yield return null;
		}
    }

    private void SetPinDirection(PinDirection o)
    {
        _pinDirection = o;
        if (isOpen) Open();
        else Close();
    }

    #region Public methods

    public void Toggle()
    {
        if (!_isOpen) Open();
        else Close();
    }

    public void Open()
    {
        if (_openCoroutine != null) StopCoroutine(_openCoroutine);
        _openCoroutine = StartCoroutine(IEOpen(true));
    }

    public void Close()
    {
        if (_openCoroutine != null) StopCoroutine(_openCoroutine);
        _openCoroutine = StartCoroutine(IEOpen(false));
    }

    public void PinLeft() => SetPinDirection(PinDirection.Left);
    public void PinRight() => SetPinDirection(PinDirection.Right);
    public void PinTop() => SetPinDirection(PinDirection.Top);
    public void PinBottom() => SetPinDirection(PinDirection.Bottom);

#endregion Public methods
}
