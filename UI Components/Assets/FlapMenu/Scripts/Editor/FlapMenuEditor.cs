using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FlapMenu))]
public class FlapMenuEditor : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		FlapMenu flapMenu = (FlapMenu)target;

		EditorGUILayout.Space();
		EditorGUILayout.LabelField("Infos", EditorStyles.boldLabel);
		EditorGUILayout.LabelField(string.Format("Is Open:    {0}", flapMenu.isOpen));

		EditorGUILayout.Space();
		if (GUILayout.Button("Toggle"))
		{
			flapMenu.Toggle();
		}
	}
}
