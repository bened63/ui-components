# UI Components
Some scripts for creating UI components

## Flap Menu
Menu with responsive width and height that is pinned at the edge of the screen and can be opened and closed.

## Visuals
![](images/toggle-1.gif)  
![](images/pin-direction-1.mp4)

## Usage
Drag the script onto a GameObject and setup everything in the Inspector.

## Project status
In development.
